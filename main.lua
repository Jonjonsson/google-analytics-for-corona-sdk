-- Google Analytics basic example. Please read readme.txt and comments in ga.lua
-- After adding your tracking ID in ga.init run this example while viewing the
-- realtime statistics at Google Analytics.

local ga = require("GoogleAnalytics.ga")

ga.init({
    isLive = false, 
    testTrackingID = "UA-abc123-1",  -- <-- Replace with your tracking code. If code is wrong it fails silently.
    debug = true,
})

ga.enterScene("Main scene") -- Screen tracking example

-- Make a button to send tap event
local tapCounter = 1
local button = display.newRect(display.contentCenterX,display.contentCenterX,100,100)
function button:tap()
    ga.event("Testing", "Button", "Tap", tapCounter) -- Send button tap event
    tapCounter = tapCounter + 1
end

button:addEventListener("tap")
